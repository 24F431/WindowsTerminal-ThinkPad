
# customized Windows Terminal settings

this is my customized Windows Terminal settings file  
it includes some custom key shortcuts and a bunch of color schemes

## installation

copy the content of this file to the current profile file

## customized Windows Terminal preview

![Windows Terminal preview](./Windows Terminal preview image.png)
